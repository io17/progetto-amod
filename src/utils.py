#!/usr/bin/python
# -*- coding: utf-8 -*-
from src import dualoc
from amplpy import AMPL, Environment
import itertools
import sys

#ampl_path = 'C:\\Users\\fabio\\Desktop\\amplide.mswin64'
ampl_path = '/Users/marco/Downloads/ampl'
ampl_solver = '/cplex'

def print_z(z, message):
    print(message)
    print(z)
    print("Cost solution : " + str(sum(z)) + "\n")


def print_facility(f, message):
    print(message)
    print(f)


def all_improved(z):
    improved = True

    for i in z:
        improved = improved and i[1]

    return improved


def test_all(distance_matrix, opening_vector, U, V):
    min_value = sys.maxsize
    f_min = []

    for L in range(1, len(opening_vector) + 1):

        for subset in itertools.combinations(range(1, len(opening_vector) + 1), L):

            x, indexes = dualoc.compute_primal_value(distance_matrix, subset, U, V)
            if x > min_value:
                continue
            f = dualoc.compute_opening_costs(opening_vector, subset)
            if min_value > (x + f) and x + f > 0:
                min_value = x + f
                f_min = subset

    print(min_value)
    print(f_min)
    exit(0)


def ampl_call(file):
    ampl = AMPL(Environment(ampl_path))
    ampl.read("src/ampl/ufl.mod")
    ampl.readData("src/ampl/"+file)
    ampl.setOption('solver', ampl_path + ampl_solver)
    ampl.solve()
    ampl_value = ampl.getObjective('TotalCost')
    ampl_facil = ampl.getVariable('x')
    return ampl_value.get().value(),ampl_facil.getValues()
