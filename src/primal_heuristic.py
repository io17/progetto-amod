#!/usr/bin/python3
# -*- coding: utf-8 -*-

from src import dualoc
import sys
import numpy


def find_facility_to_open(mode, distance_matrix, opening_vector, U, V, ret=False):
    facility = []

    # order facility by open cost
    if mode == 0:
        matrix = []
        for index, value in (list(enumerate(opening_vector))):
            temp = [index + 1, value]
            matrix.append(temp)
        matrix.sort(key=lambda x: x[1])
        for fac in matrix:
            facility.append(fac[0])
        return facility

    # compute mean over connection costs
    elif mode == 1:
        average = []
        threshold = []
        for i in range(U):
            temp = [row[i] for row in distance_matrix]
            threshold.append(numpy.mean(temp))

        threshold = numpy.mean(threshold)
        for i in range(U):
            temp = [row[i] for row in distance_matrix]

            count = 0
            for j in range(V):
                if temp[j] <= threshold:
                    count += 1
            average.append([i + 1, count])
        average.sort(key=lambda x: x[1], reverse=True)
        if ret:
            return average
        else:
            return [row[0] for row in average]

    # find min cost for clients
    elif mode == 2:

        minim = {}
        for client in distance_matrix:
            min_client = min(client)
            index = client.index(min_client)
            minim[index] = [(minim.get(index, [0]))[0] + 1, (minim.get(index, [0, 0]))[1] - min_client]

        minim_sorted = sorted(minim.items(), key=lambda x: x[1], reverse=True)

        for value in minim_sorted:
            facility.append(value[0] + 1)

        return facility
    # relations facility and connection costs
    elif mode == 3:
        min_open = find_facility_to_open(1, distance_matrix, opening_vector, U, V, True)

        min_open.sort(key=lambda x: x[0])

        temp_cost = []
        min1 = min(opening_vector)
        max1 = max(opening_vector)
        min2 = sys.maxsize
        max2 = 0
        for fac in min_open:
            if (min2 > fac[1]):
                min2 = fac[1]
            if (max2 < fac[1]):
                max2 = fac[1]
        for i in range(U):

            temp_cost.append([i + 1, 0.7 * ((opening_vector[i]-min1)/(max1-min1)) + 0.3 * (1-((min_open[i][1]-min2)/(max2-min2)))])

        temp_cost.sort(key=lambda x: x[1])
        return [row[0] for row in temp_cost]


def primal_heuristic(mode, distance_matrix, opening_vector, U, V):
    primal_facility = find_facility_to_open(mode, distance_matrix, opening_vector, U, V)
    temp = []
    min_value = [sys.maxsize, []]
    curr_indexes = []

    for value in primal_facility:
        temp.append(value)
        x, indexes = dualoc.compute_primal_value(distance_matrix, temp, U, V)
        f = dualoc.compute_opening_costs(opening_vector, temp)
        if x + f < min_value[0]:
            min_value[0] = x + f
            min_value[1] = temp[:]
            curr_indexes = indexes

    for fac in min_value[1]:
        if fac not in curr_indexes:
            min_value[0] -= opening_vector[fac - 1]
            min_value[1].pop(min_value[1].index(fac))
    return min_value
