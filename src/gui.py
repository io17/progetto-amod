import tkinter as tk
import src.data_utils as data
from src import utils, primal_heuristic
from src import dualoc


class AmodGUI:
    distance_matrix = []
    opening_vector = []
    U = 0
    V = 0

    def init_value(self, file):
        global distance_matrix
        global opening_vector
        global U
        global V
        distance_matrix, opening_vector, U, V = data.readData(file)

    def __init__(self):
        # Set values
        self.valuesComboBox = data.get_resources()
        self.valuesAlgorithm = ["Dualoc", "Min opening cost", "Avg connection cost", "Min clients cost",
                                "Min weighted opening-connection cost", "AMPL"]
        # Prepare gui
        self.prepare_gui()

    def ampl_compute(self,file):
        return utils.ampl_call(file)

    def compute(self, sequential, approx):

        z = dualoc.compute_initial_value(distance_matrix)
        z, w = dualoc.dualoc_algorithm(distance_matrix, opening_vector, U, V, z, sequential, approx)
        facility = dualoc.find_facility_to_open(U, opening_vector, w)
        x, a = dualoc.compute_primal_value(distance_matrix, facility, U, V)
        f = dualoc.compute_opening_costs(opening_vector, facility)
        return z, facility, x + f

    def compute_heuristic(self, value):
        result = primal_heuristic.primal_heuristic(value, distance_matrix, opening_vector, U, V)
        result[1].sort()
        return result

    def prepare_gui(self):
        # root frame
        self.root = tk.Tk()
        self.root.title("AMOD-GUI")
        self.root.resizable(False, False)

        # menu left
        menu_left = tk.Frame(self.root, width=250, bg="#dfdfdf")

        # upper: input area
        menu_left_upper = tk.Frame(menu_left, width=250, height=150, bg="#dfdfdf")
        test = tk.Label(menu_left_upper, text="Select input:\t\t", bg="#dfdfdf")
        test.pack(anchor=tk.NW, fill=tk.X)
        # combobox
        self.initialize_combobox(menu_left_upper, self.valuesComboBox)

        # algorithm section
        text_heur = tk.Label(menu_left_upper, text="Select heuristic:\t\t", bg="#dfdfdf")
        text_heur.pack(anchor=tk.NW, fill=tk.X)
        self.varAlg = tk.StringVar()
        self.option = tk.OptionMenu(menu_left_upper, self.varAlg, *self.valuesAlgorithm, command=self.show_hide_dualoc)
        self.varAlg.set(self.valuesAlgorithm[0])
        self.option.pack(anchor=tk.NW, fill=tk.X)

        # checkbutton
        self.check = tk.BooleanVar()
        self.checkbutton_standard = tk.Checkbutton(menu_left_upper, text="Standard algorithm          ", bg="#dfdfdf",
                                                   variable=self.check)

        self.check_approx = tk.BooleanVar()
        self.checkbutton_alg = tk.Checkbutton(menu_left_upper, text="Use better approximation", bg="#dfdfdf",
                                              variable=self.check_approx)

        self.checkbutton_standard.pack(anchor=tk.NW, fill=tk.X)
        self.checkbutton_alg.pack(anchor=tk.NW, fill=tk.X)

        menu_left_upper.pack(side="top", fill="both", expand=True, padx=3, pady=3)

        # lower: output area
        menu_left_lower = tk.Frame(menu_left, width=250, bg="#dfdfdf")
        txtOutput = tk.Label(menu_left_lower, text="Output:\t\t\t", bg="#dfdfdf")
        txtOutput.pack(anchor=tk.SW, fill=tk.X)
        menu_left_lower.pack(side="top", fill="both", expand=True)

        # right area
        start_frame = tk.Frame(self.root, bg="#dfdfdf")
        # text area
        self.canvas_area = tk.Text(self.root, background="#ffffff")
        self.canvas_area.config(state=tk.DISABLED)
        self.canvas_area.grid(row=1, column=1)
        # scroll bar
        scrollBar = tk.Scrollbar(self.root, bg="#dfdfdf")
        scrollBar.grid(row=1, column=2, sticky="nsew")
        scrollBar.config(command=self.canvas_area.yview)
        self.canvas_area.config(yscrollcommand=scrollBar.set)

        btnStart = tk.Button(start_frame, text="Start", height=2, width=20, borderwidth=3,
                             command=self.start_computation)
        btnClear = tk.Button(menu_left_lower, text="Clear", borderwidth=3, command=self.clear_textarea)
        btnSave = tk.Button(menu_left_lower, text="Save", borderwidth=3, command=self.save_output)
        btnStartAll = tk.Button(start_frame, text="Start All", height=2, width=20, borderwidth=3,
                                command=self.start_computation_all)
        btnStartAll.pack(anchor=tk.E, padx=3, pady=3, side=tk.RIGHT)
        btnStart.pack(anchor=tk.E, padx=3, pady=3, side=tk.RIGHT)
        btnClear.pack(anchor=tk.SW, fill=tk.X, padx=3, pady=3)
        btnSave.pack(anchor=tk.SW, fill=tk.X, padx=3)

        menu_left.grid(row=0, column=0, rowspan=2, sticky="nsew")
        self.canvas_area.grid(row=1, column=1, sticky="nsew")
        start_frame.grid(row=2, column=0, columnspan=3, sticky="ew")

        self.root.grid_rowconfigure(1, weight=1)
        self.root.grid_columnconfigure(1, weight=1)

    def show_hide_dualoc(self, event):
        if self.valuesAlgorithm.index(self.varAlg.get()) == 0:
            self.checkbutton_standard.pack(anchor=tk.NW, fill=tk.X)
            self.checkbutton_alg.pack(anchor=tk.NW, fill=tk.X)
        else:
            self.checkbutton_standard.pack_forget()
            self.checkbutton_alg.pack_forget()

    def initialize_combobox(self, frame, values):
        self.var = tk.StringVar()
        option = tk.OptionMenu(frame, self.var, *values)
        self.var.set(values[0])
        option.pack(anchor=tk.NW, fill=tk.X)

    def start_computation(self):

        value = self.valuesAlgorithm.index(self.varAlg.get())

        self.init_value(self.var.get())

        self.canvas_area.config(state=tk.NORMAL)

        self.canvas_area.insert(tk.END,
                                "--------------------------------------------------------------------------------\n")
        self.canvas_area.insert(tk.END, "Mode: " + self.varAlg.get())

        self.canvas_area.insert(tk.END, (
                                            " Enabled" if value == 0 and self.check.get() else " Disable" if value == 0 else " ") + " smart next choice " if value == 0 else "")
        self.canvas_area.insert(tk.END, (
                                            " Enabled" if value == 0 and self.check_approx.get() else " Disable" if value == 0 else " ") + " spread increments " if value == 0 else "")

        self.canvas_area.insert(tk.END, "\n\n")

        if value == 0:
            z, facility, x = self.compute(self.check.get(), self.check_approx.get())

            self.canvas_area.insert(tk.END, "Dualoc facility to open: \n" + str(facility) + "\n\n")
            self.canvas_area.insert(tk.END, "Cost dualoc solution : " + str(sum(z)) + "\n\n")
            self.canvas_area.insert(tk.END, "Primal solution for dualoc facility: " + str(x) + "\n\n")

        elif value == 5:
            try:
                result, facil = self.ampl_compute(self.var.get())
                self.canvas_area.insert(tk.END, "AMPL facility to open: \n" + str(facil) + "\n\n")
                self.canvas_area.insert(tk.END, "AMPL value: \n" + str(result) + "\n\n")
            except:
                self.canvas_area.insert(tk.END, "Error AMPL, check your license\n\n")
        else:
            heuristic = self.compute_heuristic(value - 1)
            self.canvas_area.insert(tk.END, "Heuristic facility to open: \n" + str(heuristic[1]) + "\n\n")
            self.canvas_area.insert(tk.END, "Heuristic value: " + str(heuristic[0]) + "\n\n")

        self.canvas_area.config(state=tk.DISABLED)

    def start_computation_all(self):

        self.init_value(self.var.get())

        self.canvas_area.config(state=tk.NORMAL)

        self.canvas_area.insert(tk.END,
                                "--------------------------------------------------------------------------------\n")
        self.canvas_area.insert(tk.END, "Mode: Dualoc\n\n")

        z, facility, x = self.compute(False, False)

        self.canvas_area.insert(tk.END, "Dualoc facility to open: \n" + str(facility) + "\n\n")
        self.canvas_area.insert(tk.END, "Cost dualoc solution : " + str(sum(z)) + "\n\n")
        self.canvas_area.insert(tk.END, "Primal solution for dualoc facility: " + str(x) + "\n\n")
        self.canvas_area.insert(tk.END,
                                "--------------------------------------------------------------------------------\n")
        self.canvas_area.config(state=tk.DISABLED)
        self.canvas_area.config(state=tk.NORMAL)
        self.canvas_area.insert(tk.END, "Mode: Dualoc with better spread\n\n")

        z, facility, x = self.compute(True, False)

        self.canvas_area.insert(tk.END, "Dualoc facility to open: \n" + str(facility) + "\n\n")
        self.canvas_area.insert(tk.END, "Cost dualoc solution : " + str(sum(z)) + "\n\n")
        self.canvas_area.insert(tk.END, "Primal solution for dualoc facility: " + str(x) + "\n\n")
        self.canvas_area.config(state=tk.DISABLED)
        self.canvas_area.config(state=tk.NORMAL)
        for i in range(len(self.valuesAlgorithm) - 2):
            self.canvas_area.insert(tk.END,
                                    "--------------------------------------------------------------------------------\n")
            heuristic = self.compute_heuristic(i)
            self.canvas_area.insert(tk.END, "Mode: Heuristic ["+str(i+1)+"]\n\n")
            self.canvas_area.insert(tk.END, "Heuristic facility to open: \n" + str(heuristic[1]) + "\n\n")
            self.canvas_area.insert(tk.END, "Heuristic value: " + str(heuristic[0]) + "\n\n")
            self.canvas_area.config(state=tk.DISABLED)
            self.canvas_area.config(state=tk.NORMAL)

        self.canvas_area.insert(tk.END,
                                "--------------------------------------------------------------------------------\n")
        try:
            self.canvas_area.insert(tk.END, "Mode: AMPL\n\n")
            result,facil = self.ampl_compute(self.var.get())
            self.canvas_area.insert(tk.END, "AMPL facility to open: \n" + str(facil) + "\n\n")
            self.canvas_area.insert(tk.END, "AMPL value: \n" + str(result) + "\n\n")
        except:
            self.canvas_area.insert(tk.END, "Error AMPL, check your license\n\n")


        self.canvas_area.config(state=tk.DISABLED)

    def append_to_textarea(self, string):
        self.canvas_area.config(state=tk.NORMAL)
        self.canvas_area.insert(tk.END, string + "\n")
        self.canvas_area.config(state=tk.DISABLED)

    def clear_textarea(self):
        self.canvas_area.config(state=tk.NORMAL)
        self.canvas_area.delete(1.0, tk.END)
        self.canvas_area.config(state=tk.DISABLED)

    def save_output(self):
        self.canvas_area.config(state=tk.NORMAL)
        console = self.canvas_area.get(1.0, tk.END)
        self.canvas_area.config(state=tk.DISABLED)
        data.save_output(console)

    def input(self):
        self.canvas_area.config(state=tk.NORMAL)
        for i in range(1, 50):
            self.canvas_area.insert(tk.END, self.var.get())
        self.canvas_area.config(state=tk.DISABLED)

    def start(self):
        self.root.mainloop()


if __name__ == "__main__":
    gui = AmodGUI()
    gui.start()
