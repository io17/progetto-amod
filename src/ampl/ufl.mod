set facility;
set customers;
param N; # number of customers
param M; # number of facilities
param f {j in facility}; 
param c {i in customers, j in facility};

var y {i in customers, j in facility} binary;  
var x{j in facility} binary; 

minimize TotalCost:
   sum {i in customers, j in facility} c[i,j]*y[i,j] + 
   sum {j in facility} f[j] * x[j];

subject to MeetDemand {i in customers}:  
   sum {j in facility} y[i,j] >= 1;

subject to IfOpen {i in customers, j in facility}:
   y[i,j] <= x[j];
