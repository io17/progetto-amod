#!/usr/bin/python3
# -*- coding: utf-8 -*-
import csv
import os
import datetime

storage_folder = 'src/resources/'
ampl_storage_folder = 'src/ampl/'

system_separator_char = " "
split_char = '\n'


# read csv
def readData(filename):
    # get file content
    csvfile = open(storage_folder + filename, "r")
    content = csvfile.read()
    csvfile.close()

    # read as array of rows
    row_splitted = content.split(split_char)

    final_result = []

    # split each row
    for row in row_splitted:

        # avoid wrong empty lines
        if (row != ''):
            splitted_row = row.split()

            final_result.append(splitted_row)

    U = int(final_result[0][0])
    V = int(final_result[0][1])

    opening_vector = []
    distance_matrix = [[0 for x in range(U)] for y in range(V)]

    for i in range(U):
        opening_vector.append((int(final_result[i + 1][1])))
        for j in range(V):
            distance_matrix[j][i] = int(final_result[1 + U + 1 + i][j])

    return distance_matrix, opening_vector, U, V


def get_resources():
    return os.listdir("src/resources")


def save_output(content):
    if not os.path.exists("results"):
        os.makedirs("results")

    filename = os.path.join("results", datetime.datetime.now().strftime('%Y%m%d_%H%M%S') + ".txt")

    file = open(filename, 'w+')
    file.write(content)
    file.close()


def write_dat_for_ampl(distance_matrix, opening_vector, U, V, file):
    file = open(ampl_storage_folder + file, "w")

    file.write("data;\n\n")

    file.write("set facility:=")
    for i in range(1, U + 1):
        file.write("F" + str(i) + ("" if i == U else " "))
    file.write(";\n")

    file.write("set customers:=")
    for i in range(1, V + 1):
        file.write("C" + str(i) + ("" if i == V else " "))
    file.write(";\n")

    file.write("param N:= " + str(V) + ";\n")

    file.write("param M:= " + str(U) + ";\n")

    file.write("\n\n")

    file.write("param:  f:=\n")
    for i in range(0, U):
        file.write("        F" + str(i + 1) + "  " + str(opening_vector[i]) + (";" if i == U - 1 else "") + "\n")
    file.write("\n")

    file.write("param c:  ")
    for i in range(1, U+1):
        file.write("F" + str(i) + "  " + (":=" if i == U else ""))
    file.write("\n")

    for i in range(1, V+1):
        file.write("      C" + str(i) + "  ")
        for j in range(1, U+1):
            file.write(str(distance_matrix[i-1][j-1])+(";" if i == V and j == U else "  "))
        file.write("\n")



    file.write("\n")


    file.close()
