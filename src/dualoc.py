#!/usr/bin/python3
# -*- coding: utf-8 -*-
from src import utils
import sys


def pick_next(z, distance_matrix, U, V):
    minim = [sys.maxsize, -1]
    for v in range(V):
        if not z[v][1]:
            for u in range(U):
                if distance_matrix[v][u] <= z[v][0]:
                    if minim[0] >= u:
                        minim = [u, v]
                    break
    return minim[1]


def compute_initial_value(distance_matrix):
    z = []
    for client in distance_matrix:
        z.append(min(client))
    return z


def compute_primal_value(distance_matrix, facility, U, V):
    z = []
    indexes = []
    facility = sorted(facility)
    new_distance_matrix = list(map(list, distance_matrix))
    for h in range(U - 1, -1, -1):
        if not (h + 1 in facility):
            for client in range(V):
                del new_distance_matrix[client][h]
    for i, client in (list(enumerate(new_distance_matrix))):
        if len(client) > 0:
            temp = min(client)
            z.append(temp)
            indexes.append(distance_matrix[i].index(temp)+1)
    return sum(z), indexes


def compute_opening_costs(opening_vector, facility):
    cost = 0
    for index, value in (list(enumerate(opening_vector))):
        if index + 1 in facility:
            cost += value
    return cost


def found_best_increment(index, distance_matrix, opening_vector, z, U, V, w, spread_algorithm):
    new_z = sys.maxsize

    min_increment = sys.maxsize

    for u in range(U):

        # distance_matrix[index][u] > z[index][0] and min_increment >= distance_matrix[index][u] and ALGORITHM:
        if z[index][0] < distance_matrix[index][u] <= min_increment and spread_algorithm:
            min_increment = distance_matrix[index][u]

        # Csu + Fu
        new_val = distance_matrix[index][u] + opening_vector[u]

        # Cus + Fu - (Ʃ Wuv)
        for v in range(V):

            # skip v = s
            if v == index:
                continue

            # Wuv = max{0, Zv - Cvu }
            new_val -= max(0, z[v][0] - distance_matrix[v][u])

        if new_val < new_z:
            new_z = new_val

    # print(index)
    # print(new_z)
    # print(min_increment)
    # print()

    new_z = min(new_z, min_increment)

    for u in range(U):
        w[index][u] = max(0, new_z - distance_matrix[index][u])

    return [new_z, True], w


def dualoc_algorithm(distance_matrix, opening_vector, U, V, z, use_next_std=False, spread_algorithm=False):
    for i in range(V):
        z[i] = [z[i], False]

    w = [[0 for col in range(U)] for row in range(V)]

    index = 0

    while not utils.all_improved(z):

        if not use_next_std:
            index = pick_next(z, distance_matrix, U, V)

        z[index], w = found_best_increment(index, distance_matrix, opening_vector, z, U, V, w, spread_algorithm)

        if use_next_std:
            index += 1

    for i in range(V):
        z[i] = z[i][0]

    return z, w


def find_facility_to_open(U, opening_vector, w):
    facility = []

    for u in range(U):
        if opening_vector[u] <= sum([row[u] for row in w]):
            facility.append(u + 1)

    return facility
