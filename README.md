## UFL heuristics and DUALOC

Python application to compute `upper/lower bounds` to UFL instances through 4 heuristics for `primal` problem and `DUALOC` heuristic for `dual` problem

`AMPL` interface to get `optimal` solution value to UFL instances

---------------------------------------------------------------------------------------

### Configuration

Install `Python3`

Download the project and `AMPL`

Set `ampl_path` and `ampl_solver` variables in `src/utils.py` to select an AMPL solver. (Suggested: `/cplex`)


---------------------------------------------------------------------------------------

### Run

Open a terminal window in the project directory and run:  `python3 main.py`