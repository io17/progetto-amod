#!/usr/bin/python3
# -*- coding: utf-8 -*-

from src import data_utils
from src.gui import AmodGUI
from src import utils
from src import dualoc
from src import primal_heuristic

distance_matrix = []
opening_vector = []
U = 0
V = 0
GUI = True
WRITE_DAT = False
#file = "A80-400-5.dat"
file = "test.dat"
#file = "A60-300-1.dat"
#file = "A30-200-1.dat"
#file = "A60-300-5.dat"



def init_value():
    global distance_matrix
    global opening_vector
    global U        #facility
    global V        #clients

    distance_matrix, opening_vector, U, V = data_utils.readData(file)


if __name__ == '__main__':
    if GUI:
        gui = AmodGUI()
        gui.start()
    else:
        ampl_res, facil = utils.ampl_call(file)
        init_value()
        #test all possibility
        #utils.test_all(distance_matrix, opening_vector, U, V)

        if WRITE_DAT:
            data_utils.write_dat_for_ampl(distance_matrix, opening_vector, U, V, file)
            exit(1)

        z = dualoc.compute_initial_value(distance_matrix)
        #utils.print_z(z, "Initial value for z ")
        z, w = dualoc.dualoc_algorithm(distance_matrix, opening_vector, U, V, z)
        facility = dualoc.find_facility_to_open(U, opening_vector, w)
        utils.print_z(z, "Final value for z ")
        utils.print_facility(facility, "List of facility to open")

        x, indexes = dualoc.compute_primal_value(distance_matrix, facility, U, V)
        f = dualoc.compute_opening_costs(opening_vector, facility)
        print("Primal solution: " + str(x + f) + "\n")

        result = primal_heuristic.primal_heuristic(3, distance_matrix, opening_vector, U, V)
        print("Heuristic facility to open\n" + str(sorted(result[1])))
        print("Heuristic value: " + str(result[0]))
